package mk.ukim.finki.wp.consultations.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Student {

    @Id
    @Column(name = "student_index")
    private String index;

    private String firstName;

    private String lastName;

    @ManyToMany(mappedBy = "followers")
    private List<Professor> following;

    public void follow(Professor professor) {
        this.following.add(professor);
        professor.getFollowers().add(this);
    }

    public void unFollow(Professor professor) {
        this.following.remove(professor);
        professor.getFollowers().remove(this);
    }
}
